//
//  ScrollToTop.swift
//  KabarXXI
//
//  Created by abit on 21/09/2019.
//  Copyright © 2019 Emerio-Mac2. All rights reserved.
//

import UIKit
import JJFloatingActionButton

class ScrollTop {
    
    var actionButton = JJFloatingActionButton()

    func configure (tableView: UITableView) -> JJFloatingActionButton{
        
        actionButton.handleSingleActionDirectly = true
        actionButton.buttonDiameter = 50
        actionButton.overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        actionButton.buttonImage = UIImage(named: "arrowup")
        actionButton.buttonColor = .white
        
        let item = actionButton.addItem()
        item.imageView.image = UIImage(named: "arrowup")
        
        item.action = { item in
            tableView.setContentOffset(.zero, animated: true)
        }
        return actionButton
    }
    
    func setAnchor(view: UIView) {
        if #available(iOS 11.0, *) {
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        } else {
        actionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        }
    }
    
}
