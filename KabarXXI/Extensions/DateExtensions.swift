//
//  DateExtensions.swift
//  LoginDemo
//
//  Created by Bayu Yasaputro on 04/04/18.
//  Copyright © 2018 DyCode. All rights reserved.
//

import Foundation

extension Date {
    
    static func getFormattedDate(dateStringParam: String) -> String{
       
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
//        let dateFromInputString = dateFormatter.date(from: dateStringParam)
//        dateFormatter.locale = Locale(identifier: "id")
//        dateFormatter.dateFormat = "EEEE dd-MMM-yyyy HH:mm:dd";
//
//        if(dateFromInputString != nil){
//            return dateFormatter.string(from: dateFromInputString!)
//        }
//        else{
//            debugPrint("could not convert date")
//            return "N/A"
//        }
        let dateAgo = dateFormatter.date(from: dateStringParam)
        let secondsAgo = Int(Date().timeIntervalSince(dateAgo!))
        
        let minute = 60
        let hour = 60 * minute
        let day = 24 * hour
        let week = 7 * day
        if secondsAgo < minute {
            return "\(secondsAgo) detik yang lalu"
        }
        else if secondsAgo < hour {
            return "\(secondsAgo / minute) menit yang lalu"
        }
        else if secondsAgo < day {
            return "\(secondsAgo / hour) jam yang lalu"
        }
        else if secondsAgo < week {
            return "\(secondsAgo / day) hari yang lalu"
        }
        return "\(secondsAgo / week) minggu yang lalu"
    }
      
}
