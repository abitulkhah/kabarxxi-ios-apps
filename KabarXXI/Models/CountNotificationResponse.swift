//
//  CountNotificationResponse.swift
//  KabarXXI
//
//  Created by abit on 20/09/2019.
//  Copyright © 2019 Emerio-Mac2. All rights reserved.
//

import Foundation

struct CountNotificationResponse : Codable {
    
    let message:String
    let status:Int
    let data: Int
    
}
