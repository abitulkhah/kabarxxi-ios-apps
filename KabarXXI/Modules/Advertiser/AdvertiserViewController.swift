//
//  AdvertiserViewController.swift
//  KabarXXI
//
//  Created by abit on 18/08/2019.
//  Copyright © 2019 Emerio-Mac2. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Kingfisher
import UIScrollView_InfiniteScroll
import CoreSpotlight
import MobileCoreServices
import Firebase
import GoogleMobileAds
import JJFloatingActionButton

class AdvertiserViewController: UITableViewController , GADBannerViewDelegate{
    
    
    @IBOutlet var advertiserTableView: UITableView!
    
    var advertiserArray: [Advertiser] = []
    var refreshControl_: UIRefreshControl?
    var totalPage = 0
    var page = 0
    var adsToLoad = [GADBannerView]()
    var loadStateForAds = [GADBannerView: Bool]()
    let adUnitID = Constant.AdmobAccount
    let adInterval = UIDevice.current.userInterfaceIdiom == .pad ? 16 : 8
    let adViewHeight = CGFloat(100)
    var indicator = UIActivityIndicatorView()
    var tableViewItems : [Any] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setupViews()
        refreshControl_!.beginRefreshing()
        loadAdvertiser(page)
        
        
    }
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width:40, height:40))
        indicator.style = UIActivityIndicatorView.Style.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadAdvertiser(page)
    }
    
    @objc func refresh(_ sender: UIRefreshControl) {
        
        loadAdvertiser(page)
        
    }
    
    func registerCell(){
        
        tableView.register(UINib(nibName: "AdvertiserTableViewCell", bundle: nil),forCellReuseIdentifier:  "AdvertiserTableViewCell")
        
        tableView.register(UINib(nibName: "BannerAd", bundle: nil),
                           forCellReuseIdentifier: "BannerViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 135
    }
    
    
    
    func adViewDidReceiveAd(_ adView: GADBannerView) {
        print("received ads")
        loadStateForAds[adView] = true
        preloadNextAd()
    }
    
    func adView(_ adView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("Failed to receive ad: \(error.localizedDescription)")
        preloadNextAd()
    }
    
    
    func addBannerAds() {
        print("add banner")
        var index = adInterval
        tableView.layoutIfNeeded()
        while index < tableViewItems.count {
            let adSize = GADAdSizeFromCGSize(
                CGSize(width: tableView.contentSize.width, height: adViewHeight))
            let adView = GADBannerView(adSize: adSize)
            adView.adUnitID = adUnitID
            adView.rootViewController = self
            adView.delegate = self
            tableViewItems.insert(adView, at: index)
            adsToLoad.append(adView)
            loadStateForAds[adView] = false
            index += adInterval
        }
    }
    
    func preloadNextAd() {
        print("preload ads")
        if !adsToLoad.isEmpty {
            let ad = adsToLoad.removeFirst()
            let adRequest = GADRequest()
            //            adRequest.testDevices = [ kGADSimulatorID ]
            print("testing ads")
            ad.load(adRequest)
        }
    }
    
    func setupViews() {
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        advertiserTableView.addSubview(refreshControl)
        self.refreshControl_ = refreshControl
        advertiserTableView.addInfiniteScroll { (advertiserTableView) in
            self.loadAdvertiser(self.page + 1)
        }
        
        advertiserTableView.setShouldShowInfiniteScrollHandler { (advertiserTableView) -> Bool in
            return self.page < self.totalPage
        }
        
        scrollToTop()
    }
    
    func scrollToTop(){
        
        let actionButton = JJFloatingActionButton()
        
        actionButton.handleSingleActionDirectly = true
        actionButton.buttonDiameter = 50
        actionButton.overlayView.backgroundColor = UIColor(white: 0, alpha: 0.3)
        actionButton.buttonImage = UIImage(named: "arrowup")
        actionButton.buttonColor = .white
        
        self.advertiserTableView.addSubview(actionButton)
        actionButton.display(inViewController: self)
        
        let item = actionButton.addItem()
        item.imageView.image = UIImage(named: "arrowup")
        item.action = { item in
            self.advertiserTableView.setContentOffset(.zero, animated: true)
        }
    }
    
    func loadAdvertiser(_ page:Int) {
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.darkGray
        advertiserProviderServices.request(.getAdvertiser(page)) { [weak self] result in
            guard case self = self else { return }
            
            // 3
            switch result {
            case .success(let response):
                do {
                    
                    let decoder = JSONDecoder()
                    let responses = try decoder.decode(AdvertiserResponse.self, from:
                        response.data)
                    
                    if page == 0 {
                        
                        self?.tableViewItems = responses.data ?? []
                        
                    }
                    else {
                        
                        self?.tableViewItems.append(contentsOf: responses.data ?? [])
                        
                    }
                    
                    self?.totalPage = self?.tableViewItems.count ?? 0/10
                    self?.page = page
                    self?.advertiserTableView.reloadData()
                    
                } catch let parsingError {
                    print("Error", parsingError)
                }
                
            case .failure: break
            }
            
            self?.refreshControl_?.endRefreshing()
            self?.advertiserTableView.finishInfiniteScroll()
            self?.addBannerAds()
            self?.preloadNextAd()
        }
        
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewItems.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if let BannerView = tableViewItems[indexPath.row] as? GADBannerView {
            let reusableAdCell = tableView.dequeueReusableCell(withIdentifier: "BannerViewCell", for: indexPath)
            
            
            for subview in reusableAdCell.contentView.subviews {
                subview.removeFromSuperview()
            }
            
            reusableAdCell.contentView.addSubview(BannerView)
            BannerView.center = reusableAdCell.contentView.center
            return reusableAdCell
            
        }
        else {
            
            
            self.advertiserTableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
            print("tes",indexPath.row)
            let cell = Bundle.main.loadNibNamed("AdvertiserTableViewCell", owner: self, options: nil)?.first as! AdvertiserTableViewCell
            let advertiser_ = tableViewItems[indexPath.row] as? Advertiser
            let imageUrl = Constant.ApiUrlImage+"\(advertiser_?.base64Image?.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)   ?? "")"
            cell.advertiserImage.kf.setImage(with: URL(string: imageUrl), placeholder: UIImage(named: "default_image"))
            return cell
            
        }
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let tableItem = tableViewItems[indexPath.row] as? GADBannerView {
            let isAdLoaded = loadStateForAds[tableItem]
            return isAdLoaded == true ? adViewHeight : 0
        } else {
            return 200
        }
        
}
}
